!classDefinition: #Jaz category: 'Actividad2'!
DenotativeObject subclass: #Jaz
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Actividad2'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Jaz class' category: 'Actividad2'!
Jaz class
	instanceVariableNames: 'nivelDeEstres'!

!Jaz class methodsFor: 'as yet unclassified' stamp: 'lt 3/21/2022 21:20:58'!
comer: unaCantidadDeBarrasDeChocolate
	
	nivelDeEstres := nivelDeEstres - (2 * unaCantidadDeBarrasDeChocolate)! !

!Jaz class methodsFor: 'as yet unclassified' stamp: 'lt 3/21/2022 21:28:46'!
mover: cantDeCajas
	
	nivelDeEstres >= 3
		ifTrue: [
			^'NO QUIERO'	
		]
		ifFalse: [
			nivelDeEstres := nivelDeEstres + (0.5 * cantDeCajas) + 1
		]! !

!Jaz class methodsFor: 'as yet unclassified' stamp: 'lt 3/21/2022 21:19:20'!
nivelDeEstres

	^nivelDeEstres! !


!Jaz class methodsFor: '--** private fileout/in **--' stamp: 'lt 3/21/2022 21:39:09'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	nivelDeEstres := 3.0.! !

Jaz initializeAfterFileIn!